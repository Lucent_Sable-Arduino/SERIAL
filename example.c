/*
 *  uart.c
 *
 *  By S. J. Gosnell 
 *
 *  Demonstrates the UART receive transmission code
 *
 *  (C) 2013 The University of Waikato
 */

#include <string.h>
#include <stdio.h>
#include <lcd.h>
#include <serial.h>

void trim_nl(char* str)
{
	int l = strlen(str);
	if(str[l-1]=='\n'||str[l-1]=='\r')
		str[l-1]=0;
}

int main(void)
{
	//make the line buffers one longer to allow
	//for a terminating 0
    //buffer for holding line 1
    char line1[17];
    //string to output to LCD
    char outbuff[33];
    char line2[17];

    lcd_init();
    serial_init();
    lcd_clear();

    while(1)
    {
		//scroll upward
		snprintf(line1,17,"%s",line2);
		//get a line from stdin (serial)
		fgets(line2,17,stdin);
		//remove the trailing newline if it exists
		trim_nl(line2);
		//copy lines into a buffer
		snprintf(outbuff,33,"%s\n%s",line1,line2);
		//echo the newest line to the serial terminal
		printf("%s\n",line2);
		int i = 0;
		//output line2 and line1 to the LCD
		lcd_display(outbuff);
    }
    return 0;
}
