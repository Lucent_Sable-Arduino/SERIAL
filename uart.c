/*
 *  uart.c
 *
 *  By S. J. Gosnell 
 *
 *  Demonstrates the UART receive transmission code
 *
 *  (C) 2013 The University of Waikato
 */

#include <string.h>
#include <stdio.h>
#include <lcd.h>
#include <avr/io.h>
#include <util/delay.h>

/*Define baud to be 9600 bps*/
#define BAUD 103 

 
void serial_init()
{
	/*Set baud rate*/
	UBRR0H = (unsigned char)(BAUD>>8);
	UBRR0L = (unsigned char)(BAUD);
	/*Enable transmit and recieve*/
	UCSR0B = (1<<RXEN0)|(1<<TXEN0);
	/*set frame format 8 data,even parity, 1 stop
	 *	UCSZ00 - Data Length
	 *	UPM01  - Parity
	 *Defaults to 1 stop bit
	 */
	UCSR0C = ((3<<UCSZ00)|(1<<UPM01));
}

int serial_RX_byte()
{
	//return 0 if the data is not ready
	if(!(UCSR0A & (1<<RXC0)))
		return -1;
	//return the data
	return UDR0;
}

void serial_TX_byte(char data)
{
	//await empty transmit buffer
	while(!(UCSR0A&(1<<UDRE0)));
	//transmit the character
	UDR0 = data;
}

void serial_TX_string(char* str)
{
	//read until null character
	while(*str)
	{
		//transmit the data
		serial_TX_byte(*(str++));
	}
}

int serial_RX_string(char* str, int len)
{
	//len is the length of the string, including null terminator
	int i = 0;
	unsigned long j = 0;
	unsigned long timeout = 800000L;
	int c;

	//for some reason, some characters are recieved with 0x80 added to them
	//this needs to be accounted for int the line recieve code for newlines
	while((i<(len-1))
		&&((char)c!='\r')
		&&((char)c != 0xFF8D))
	{	
		c=-1;
		//wait for character to cone in, or timeout
		for(j=0;j<timeout && c==-1;j++)c=serial_RX_byte();
		//if there is a character, append to the string
		if(j<timeout)
		{
			//only add a character if it is not a special character
			//the first non-special character is space (0x20)
			//also account for the +0x80 bug
			//all special characters are spaces now.
			if((c<0x20)||(c>0x80&&c<0x20+0x80))
				*(str++)=' ';
			else
				*(str++)=(char)c;
		}
		//otherwise timeout, return 0
		else
		{
			//terminate line
			*(str++)=0;
			//return true if there are characters on line
			 return i>0;
		}
		i++;
	}
	//buffer was filled or new line, return true
	//replace last character with line termination
		//(either a newline or the last element)
	*(str)=0;
	//return true (there are characters to write)
	return 1;
}

int main(void)
{
	//make the line buffers one longer to allow
	//for a terminating 0
    //buffer for holding line 1
    char line1[17];
    //old version of line 1 (for vertical scrolling
    char buff[17];
    //string to output to LCD
    char outbuff[33];
    //line2
    char line2[17];

    lcd_init();
    serial_init();
    lcd_clear();

    while(1)
    {
	//wait for a line to be recieved
	if(serial_RX_string(line1,17))
	{
		serial_TX_string(line1);
		serial_TX_string("\r\n");
		//when a line is recieved
		//copy the previous contents of line1 to line2
		snprintf(line2,17,"%s",buff);
		//copy the current contents of line1 into a buffer
		snprintf(buff,17,"%s",line1);
		//copy line2 and line1 into a buffer
		snprintf(outbuff,33,"%s\n%s",line2,line1);
		//output line2 and line1 to the LCD
		lcd_display(outbuff);
	}
    }
    return 0;
}
