/*
 *  uart.c
 *
 *  By S. J. Gosnell 
 *
 *  Demonstrates the UART receive transmission code
 *
 *  (C) 2013 The University of Waikato
 */

#include <string.h>
#include <stdio.h>
#include <avr/io.h>
#include <serial.h>

/*Define baud to be 9600 bps*/
#define BAUD 103 

 
void serial_init();
static void serial_TX_byte(char data);
static char serial_RX_byte();

static int ser_putchar(char c, FILE *f);

static char ser_getchar(FILE *f);

static FILE my_stdio = FDEV_SETUP_STREAM(ser_putchar, ser_getchar, _FDEV_SETUP_RW);


static int ser_putchar(char c, FILE *f)
{
	serial_TX_byte(c);
	if(c=='\r')serial_TX_byte('\n');
	if(c=='\n')serial_TX_byte('\r');
}

static char ser_getchar(FILE *f)
{
	//int res = -1;
	//while(res==-1)res = serial_RX_byte();
	//return res;
	char c = serial_RX_byte()&~0xFF80;

	if(c == '\r')c='\n';
	return c;	
}


void serial_init()
{
	/*Set baud rate*/
	UBRR0H = (unsigned char)(BAUD>>8);
	UBRR0L = (unsigned char)(BAUD);
	/*Enable transmit and recieve*/
	UCSR0B = (1<<RXEN0)|(1<<TXEN0);
	/*set frame format 8 data,even parity, 1 stop
	 *	UCSZ00 - Data Length
	 *	UPM01  - Parity
	 *Defaults to 1 stop bit
	 */
	UCSR0C = ((3<<UCSZ00)|(1<<UPM01));
	stdout = &my_stdio;
	stdin  = &my_stdio;
}

static char serial_RX_byte()
{
	//return 0 if the data is not ready
	//if(!(UCSR0A & (1<<RXC0)))
		//return -1;
	while(!(UCSR0A & (1<<RXC0)));
	//return the data
	return UDR0;
}
static void serial_TX_byte(char data)
{
	//await empty transmit buffer
	while(!(UCSR0A&(1<<UDRE0)));
	//transmit the character
	UDR0 = data;
}
