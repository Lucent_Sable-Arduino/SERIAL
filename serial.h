/*
 * uart.h
 * S.J.Gosnell
 * Header file for public functions from the UART library
 *
 * Public Functions:
 *	serial_init()
 *
 * To use the library, use stdio functions such as printf() and fgets()
 */

//initialises the serial port for use
extern void serial_init();
